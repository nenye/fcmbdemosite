<!-- #Footer Area -->

<?php global $irex_shortname; ?>

<div id="footer-area">
  <div class="wrapper container_24 clearfix">
    <div id="footer" class="page clearfix" >
      <div id="site-info"> <span>
        <?php  if(sketch_get_option($irex_shortname."_copyright")){ echo sketch_get_option($irex_shortname."_copyright");}  ?>
        </span>
        <div class="grid_12 alpha omega">Irex Theme by <a class="irex-link" href="http://www.sketchthemes.com/" title="Sketch Themes">SketchThemes</a></div>
      </div>
      <div id="foot-nav">
        <?php 

				if( function_exists( 'has_nav_menu' ) && has_nav_menu( 'footer' ) ) {

					wp_nav_menu(array( 'container_class' => 'menu-footer-container', 'menu_id' => 'menu-footer', 'theme_location' => 'footer', 'depth' => 1 ));

				} else { ?>
        <div class="menu-footer-container">
          <ul id="menu-footer" class="menu">
            <?php wp_list_pages('title_li=&depth=1'); ?>
          </ul>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<!-- #Footer Area --> 
<a href="JavaScript:void(0);" title="Back To Top" id="back-top" class="">
<?php _e('Back To Top','irex'); ?>
</a>
<?php wp_footer(); ?>
</body></html>