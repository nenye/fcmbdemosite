<!-- #search -->
<div id="search">
  <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>/">
    <input type="text" value="Search keywords" name="s" id="searchbox" onfocus="this.value=''" class="searchinput"/>
    <input type="submit" class="submitbutton" value="Go" />
  </form>
</div>
<!-- #search --> 