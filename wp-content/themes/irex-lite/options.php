<?php

/**

 * A unique identifier is defined to store the options in the database and reference them from the theme.

 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.

 * If the identifier changes, it'll appear as if the options have been reset.

 */

function optionsframework_option_name() {

	global $irex_shortname;

	global $irex_themename;

	// This gets the theme name from the stylesheet

	$irex_themename = get_option( 'stylesheet' );

	$irex_themename = preg_replace("/\W/", "_", strtolower($irex_themename) );

	$optionsframework_settings = get_option( 'optionsframework' );

	$optionsframework_settings['id'] = $irex_themename;

	update_option( 'optionsframework', $optionsframework_settings );

	//setting contact us page

	if(sketch_get_option($irex_shortname.'_contact_page'))

		select_template(sketch_get_option($irex_shortname.'_contact_page'));

}

/**

 * Defines an array of options that will be used to generate the settings page and be saved in the database.

 * When creating the 'id' fields, make sure to use all lowercase and no spaces.

 *

 * If you are making your theme translatable, you should replace 'irex'

 * with the actual text domain for your theme.  Read more:

 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain

 */

function optionsframework_options() {

	global $irex_shortname;

	global $irex_themename;
	
	$test_colorarray = array(

		'green' => __('Green(Dark Skin)', 'irex'),

		'red' => __('Red(Light Skin)', 'irex'),

	);

	//effect_test_array

		$effect_test_array = array(

			'random' => __('Random', 'irex'),

			'simpleFade' => __('SimpleFade', 'irex'),

			'curtainTopLeft' => __('CurtainTopLeft', 'irex'),

			'curtainTopRight' => __('CurtainTopRight', 'irex'),

			'curtainBottomLeft' => __('CurtainBottomLeft', 'irex'),

			'curtainBottomRight' => __('CurtainBottomRight', 'irex'),

			'curtainSliceLeft' => __('CurtainSliceLeft', 'irex'),

			'curtainSliceRight' => __('CurtainSliceRight', 'irex'),

			'blindCurtainTopLeft' => __('BlindCurtainTopLeft', 'irex'),

			'blindCurtainTopRight' => __('BlindCurtainTopRight', 'irex'),

			'blindCurtainBottomLeft' => __('BlindCurtainBottomLeft', 'irex'),

			'blindCurtainBottomRight' => __('BlindCurtainBottomRight', 'irex'),

			'blindCurtainSliceBottom' => __('BlindCurtainSliceBottom', 'irex'),

			'blindCurtainSliceTop' => __('BlindCurtainSliceTop', 'irex'),

			'stampede' => __('Stampede', 'irex'),

			'mosaic' => __('Mosaic', 'irex'),

			'mosaicReverse' => __('MosaicReverse', 'irex'),

			'mosaicRandom' => __('MosaicRandom', 'irex'),

			'mosaicSpiral' => __('MosaicSpiral', 'irex'),

			'mosaicSpiralReverse' => __('MosaicSpiralReverse', 'irex'),

			'topLeftBottomRight' => __('TopLeftBottomRight', 'irex'),

			'bottomRightTopLeft' => __('BottomRightTopLeft', 'irex'),

			'bottomLeftTopRight' => __('BottomLeftTopRight', 'irex'),

			'mosaicSpiral' => __('MosaicSpiral', 'irex'),

			'mosaicSpiral' => __('MosaicSpiral', 'irex')

		);

	// Test data

	$test_pagiarray = array(

		1 => __('Enable', 'irex'),

		0 => __('Disable', 'irex')

	);

	// slider on off

		$slideron_array = array(

			1 => __('On', 'irex'),

			0 => __('Off', 'irex')

		);

	//navigation_array

      $navigation_array = array(

		'true' => __('Enable', 'irex'),

		'false' => __('Disable', 'irex')

	);

	//pagination_array

      $pagination_array = array(

		'true' => __('Enable', 'irex'),

		'false' => __('Disable', 'irex')

	);

	//controlNavThumbs_array

 	$controlNavThumbs_array = array(

		'true' => __('Enable', 'irex'),

		'false' => __('Disable', 'irex')

	);

	//autoadvance_array

	$autoadvance_array = array(

		'true' => __('Enable', 'irex'),

		'false' => __('Disable', 'irex')

	);

	//pauseOnHover_array

	$pauseOnhover_array = array(

		'true' => __('Enable', 'irex'),

		'false' => __('Disable', 'irex')

	);

	//playpause_array

	$playpause_array = array(

		'true' => __('Enable', 'irex'),

		'false' => __('Disable', 'irex')

	);

	// set contact pages

	$options_pages = array();

	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');

	$options_pages[''] = 'Select a page:';

	foreach ($options_pages_obj as $page) {

		$options_pages[$page->ID] = $page->post_title;

	}

	// If using image radio buttons, define a directory path

	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

	//General Settings

	$options[] = array(

		'name' => __('General Settings', 'irex'),

		'type' => 'heading');

		$options[] = array(

			'name' => __('Custom Logo :', 'irex'),

			'desc' => __('Choose your own logo for your site. Size: Width 314px and Height 50px.', 'irex'),

			'id' => $irex_shortname.'_logo_img',

			'std' => '',

			'type' => 'upload');

		$options[] = array(

			'name' => __('Logo Alt Text :', 'irex'),

			'desc' => __('Enter alternate text for logo.', 'irex'),

			'id' => $irex_shortname.'_logo_alt',

			'std' => '',

			'type' => 'text');

		$options[] = array(

			'name' => __('Custom favicon :', 'irex'),

			'desc' => __('Choose a custom favicon for your site. Size: Width:16px and Height:16px .', 'irex'),

			'id' => $irex_shortname.'_favicon',

			'std' => '',

			'type' => 'upload');
		
		$options[] = array(

			'name' => __('Choose Skin', 'irex'),

			'desc' => __('Choose theme color skin.', 'irex'),

			'id' => $irex_shortname.'_color_scheme',

			'std' => '',

			'type' => 'select',

			'options' => $test_colorarray);

	    $options[] = array(
			'name' => __('Custom Pagination', 'irex'),
			'desc' => __('Choose enable to show custom pagination on blog page.', 'irex'),
			'id' => $irex_shortname.'_show_pagination',
			'std' => 'yes',
			'type' => 'select',
			'class' => 'small', //mini, tiny, small
			'options' => $test_pagiarray);

	//Front Page Options	
	$options[] = array(
		'name' => __('Front Page Options', 'irex'),
		'type' => 'heading');

		//Front Page Middle Text

		$options[] = array(
			'name' => __('Front Page Heading Text:', 'irex'),
			'desc' => __('Enter heading text for front page.', 'irex'),
			'id' => $irex_shortname.'_mid_text',
			'std' => 'Welcome to the Portfolio of Irex Studio',
			'type' => 'text');
		
		//Featured heading
		$options[] = array(
			'name' => __('Featured Area Heading:', 'irex'),
			'desc' => __('Enter featured area heading.', 'irex'),
			'id' => $irex_shortname.'_feature_head_area',
			'std' => 'Featured Area',
			'type' => 'text');
			
		//Featured Box 1
		$options[] = array(
			'name' => __('First Featured Area Image:', 'irex'),
			'desc' => __('Choose image for first featured area. Size: Width 212px and Height 122px', 'irex'),
			'id' => $irex_shortname.'_feature_img1',
			'std' => $imagepath.'slide1.jpg',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('First Featured Area Title:', 'irex'),
			'desc' => __('Enter title for first featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_text1',
			'std' => 'Featured Box 1',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('First Featured Area Title Link:', 'irex'),
			'desc' => __('Enter link for first featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_link1',
			'std' => '#',
			'type' => 'text');
			
			//Featured Box 2
		$options[] = array(
			'name' => __('Second Featured Area Image:', 'irex'),
			'desc' => __('Choose image for second featured area. Size: Width 212px and Height 122px.', 'irex'),
			'id' => $irex_shortname.'_feature_img2',
			'std' => $imagepath.'slide2.jpg',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('Second Featured Area Title:', 'irex'),
			'desc' => __('Enter title for second featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_text2',
			'std' => 'Featured Box 2',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('Second Featured Area Title Link:', 'irex'),
			'desc' => __('Enter link for second featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_link2',
			'std' => '#',
			'type' => 'text');
			
	//Featured Box 3
		$options[] = array(
			'name' => __('Third  Featured Area Image:', 'irex'),
			'desc' => __('Choose image for third featured area. Size: Width 212px and Height 122px.', 'irex'),
			'id' => $irex_shortname.'_feature_img3',
			'std' => $imagepath.'slide3.jpg',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('Third Featured Area Title:', 'irex'),
			'desc' => __('Enter title for third featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_text3',
			'std' => 'Featured Box 3',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('Third Featured Area Title Link:', 'irex'),
			'desc' => __('Enter link for third featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_link3',
			'std' => '#',
			'type' => 'text');
	
	//Featured Box 4
		$options[] = array(
			'name' => __('Fourth Featured Area Image:', 'irex'),
			'desc' => __('Choose image for fourth featured area. Size: Width 212px and Height 122px.', 'irex'),
			'id' => $irex_shortname.'_feature_img4',
			'std' => $imagepath.'slide1.jpg',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('Fourth Featured Area Title:', 'irex'),
			'desc' => __('Enter title for fourth featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_text4',
			'std' => 'Featured Box 4',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('Fourth Featured Area Title Link:', 'irex'),
			'desc' => __('Enter link for fourth featured area.', 'irex'),
			'id' => $irex_shortname.'_feature_link4',
			'std' => '#',
			'type' => 'text');
			

	//Slider Setting

	$options[] = array(
		'name' => __('Slider Configuration', 'irex'),
		'type' => 'heading');
		
		$options[] = array(
			'name' => __('Upload First Slide Image:', 'irex'),
			'desc' => __('Choose image for front page slider. Size: Width 800px and Height 500px.', 'irex'),
			'id' => $irex_shortname.'_slider_img1',
			'std' => $imagepath.'slide1.jpg',
			'type' => 'upload');

		$options[] = array(
			'name' => __('Enter Content For First Slide:', 'irex'),
			'desc' => __('Enter content for first slide.', 'irex'),
			'id' => $irex_shortname.'_content_slider1',
			'std' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
			'type' => 'textarea');
			
		$options[] = array(
			'name' => __('Upload Second Slide Image:', 'irex'),
			'desc' => __('Choose image for front page slider. Size: Width 800px and Height 500px.', 'irex'),
			'id' => $irex_shortname.'_slider_img2',
			'std' => $imagepath.'slide2.jpg',
			'type' => 'upload');

		$options[] = array(
			'name' => __('Enter Content For Second Slide:', 'irex'),
			'desc' => __('Enter content for second slide.', 'irex'),
			'id' => $irex_shortname.'_content_slider2',
			'std' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
			'type' => 'textarea');
			
		$options[] = array(
			'name' => __('Upload Third Slide Image:', 'irex'),
			'desc' => __('Choose image for front page slider. Size: Width 800px and Height 500px.', 'irex'),
			'id' => $irex_shortname.'_slider_img3',
			'std' => $imagepath.'slide3.jpg',
			'type' => 'upload');

		$options[] = array(
			'name' => __('Enter Content For Third Slide:', 'irex'),
			'desc' => __('Enter content for third slide.', 'irex'),
			'id' => $irex_shortname.'_content_slider3',
			'std' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
			'type' => 'textarea');
			

		   $options[] = array(
				'name' => __('Slider Transition :', 'irex'),
				'desc' => __('Choose transition effect for front page slider.', 'irex'),
				'id' => $irex_shortname.'_effect_select',
				'std' => 'random',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $effect_test_array);

			$options[] = array(
				'name' => __('Animation Speed :', 'irex'),
				'desc' => __('Enter Slider animation in ms(mili second).', 'irex'),
				'id' => $irex_shortname.'_animation_speed',
				'std' => '500',
				'type' => 'text');

			$options[] = array(
				'name' => __('Slide Pause Time :', 'irex'),
				'desc' => __('Enter pause time in ms(mili second).', 'irex'),
				'id' => $irex_shortname.'_pause_time',
				'std' => '3000',
				'type' => 'text');

			$options[] = array(
				'name' => __('Slider Navigation :', 'irex'),
				'desc' => __('Choose enable to show slider navigation buttons.', 'irex'),
				'id' => $irex_shortname.'_navigation_select',
				'std' => 'true',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $navigation_array);

			$options[] = array(
				'name' => __('Slider Pagination :', 'irex'),
				'desc' => __('Choose enable to show slider Pagination.', 'irex'),
				'id' => $irex_shortname.'_paginationv_select',
				'std' => 'true',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $pagination_array);

			$options[] = array(
				'name' => __('Control Nav Thumbs :', 'irex'),
				'desc' => __('Choose enable to show Control Navigation Thumbnails.', 'irex'),
				'id' => $irex_shortname.'_controlNavThumbs_select',
				'std' => 'false',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $controlNavThumbs_array);

			$options[] = array(
				'name' => __('Slider Auto Play :', 'irex'),
				'desc' => __('Choose enable to auto play slider.', 'irex'),
				'id' => $irex_shortname.'_autoadvance',
				'std' => 'true',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $autoadvance_array);

			$options[] = array(
				'name' => __('Slider Pause On Hover :', 'irex'),
				'desc' => __('Choose enable to pause slider on mouse hover.', 'irex'),
				'id' => $irex_shortname.'_pauseonhover_select',
				'std' => 'true',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $pauseOnhover_array);

		$options[] = array(
				'name' => __('Slider Play Pause Button :', 'irex'),
				'desc' => __('Choose enable to show play pause button on slider.', 'irex'),
				'id' => $irex_shortname.'_playpause_select',
				'std' => 'true',
				'type' => 'select',
				'class' => 'small', //mini, tiny, small
				'options' => $playpause_array);

	
	//Social links

	$options[] = array(
		'name' => __('Social links Settings', 'irex'),
		'type' => 'heading');

	 //Facebook
		$options[] = array(
			'name' => __('Facebook URL:', 'irex'),
			'desc' => __('Enter your facebook url. Leave empty if do not want to show this icon.', 'irex'),
			'id' => $irex_shortname.'_fb_link',
			'std' => '#',
			'type' => 'text');

	//Twitter
		$options[] = array(
			'name' => __('Twitter URL:', 'irex'),
			'desc' => __('Enter your twitter url. Leave empty if do not want to show this icon.', 'irex'),
			'id' => $irex_shortname.'_tw_link',
			'std' => '#',
			'type' => 'text');

	//Linkedin
		$options[] = array(
			'name' => __('Linkedin URL:', 'irex'),
			'desc' => __('Enter your linkedin url. Leave empty if do not want to show this icon.', 'irex'),
			'id' => $irex_shortname.'_lkd_link',
			'std' => '#',
			'type' => 'text');

	//Google
		$options[] = array(
			'name' => __('Google+ URL:', 'irex'),
			'desc' => __('Enter your google+ url. Leave empty if do not want to show this icon.', 'irex'),
			'id' => $irex_shortname.'_gplus_link',
			'std' => '#',
			'type' => 'text');
			
	//About Page Options	
	$options[] = array(
		'name' => __('About Page Options', 'irex'),
		'type' => 'heading');
	
	//Team heading
	$options[] = array(
			'name' => __('Team Member Area Heading:', 'irex'),
			'desc' => __('Enter heading for team member area.', 'irex'),
			'id' => $irex_shortname.'_taem_head_area',
			'std' => 'Team Member Area',
			'type' => 'text');
			
	//Team Member Box 1
		$options[] = array(
			'name' => __('First Team Member Image:', 'irex'),
			'desc' => __('Upload first team member image. Size: Width 200px and Height 142px.', 'irex'),
			'id' => $irex_shortname.'_teammember_img1',
			'std' => '',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('First Team Member Title:', 'irex'),
			'desc' => __('Enter first team member title.', 'irex'),
			'id' => $irex_shortname.'_teammember_text1',
			'std' => 'Team Member 1',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('First Team Member Short Description:', 'irex'),
			'desc' => __('Enter first team member short description.', 'irex'),
			'id' => $irex_shortname.'_teammember_content1',
			'std' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
			'type' => 'textarea');
			
		$options[] = array(
			'name' => __('First Team Member Title Link:', 'irex'),
			'desc' => __('Enter first team member title link.', 'irex'),
			'id' => $irex_shortname.'_teammember_link1',
			'std' => '#',
			'type' => 'text');
			
		//Team Member Box 2
		$options[] = array(
			'name' => __('Second Team Member Image:', 'irex'),
			'desc' => __('Upload second team member image. Size: Width 200px and Height 142px.', 'irex'),
			'id' => $irex_shortname.'_teammember_img2',
			'std' => '',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('Second Team Member Title:', 'irex'),
			'desc' => __('Enter second team member title.', 'irex'),
			'id' => $irex_shortname.'_teammember_text2',
			'std' => 'Team Member 2',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('Second Team Member Short Description:', 'irex'),
			'desc' => __('Enter Second team member short description.', 'irex'),
			'id' => $irex_shortname.'_teammember_content2',
			'std' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
			'type' => 'textarea');
			
		$options[] = array(
			'name' => __('Second Team Member Title Link:', 'irex'),
			'desc' => __('Enter second team member title link.', 'irex'),
			'id' => $irex_shortname.'_teammember_link2',
			'std' => '#',
			'type' => 'text');
			
	   //Team Member Box 3
		$options[] = array(
			'name' => __('Third Team Member Image:', 'irex'),
			'desc' => __('Upload third team member image. Size: Width 200px and Height 142px.', 'irex'),
			'id' => $irex_shortname.'_teammember_img3',
			'std' => '',
			'type' => 'upload');
			
		$options[] = array(
			'name' => __('Third Team Member Title:', 'irex'),
			'desc' => __('Enter third team member title.', 'irex'),
			'id' => $irex_shortname.'_teammember_text3',
			'std' => 'Team Member 3',
			'type' => 'text');
			
		$options[] = array(
			'name' => __('Third Team Member Short Description:', 'irex'),
			'desc' => __('Enter third team member short description.', 'irex'),
			'id' => $irex_shortname.'_teammember_content3',
			'std' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
			'type' => 'textarea');
			
		$options[] = array(
			'name' => __('Third Team Member Title Link:', 'irex'),
			'desc' => __('Enter third team member title link.', 'irex'),
			'id' => $irex_shortname.'_teammember_link3',
			'std' => '#',
			'type' => 'text');
			
	//Footer	
	$options[] = array(
		'name' => __('Footer Settings', 'irex'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Footer Copyright Text:', 'irex'),
		'desc' => __('Enter your copyright text here.', 'irex'),
		'id' => $irex_shortname.'_copyright',
		'std' => 'Copyright Text Here',
		'type' => 'textarea');
		
	return $options;

}

/*

 * This is an example of how to add custom scripts to the options panel.

 * This example shows/hides an option when a checkbox is clicked.

 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>
<script type="text/javascript">

jQuery(document).ready(function($) {

	$('#example_showhidden').click(function() {

  		$('#section-example_text_hidden').fadeToggle(400);

	});

	if ($('#example_showhidden:checked').val() !== undefined) {

		$('#section-example_text_hidden').show();

	}

});

</script>
<?php

}