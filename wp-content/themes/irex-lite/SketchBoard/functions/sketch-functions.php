<?php
global $irex_themename;
global $irex_shortname;

/************* Custom Page Title ***********
*******************************************/
add_filter( 'wp_title', 'irex_title' );

function irex_title($title)
{
	$irex_title = $title;
	if ( is_home() && !is_front_page() ) {
		$irex_title .= get_bloginfo('name');
	}
	if ( is_front_page() ){
		$irex_title .=  get_bloginfo('name');
		$irex_title .= ' | '; 
		$irex_title .= get_bloginfo('description');
	}
	if ( is_search() ) {
		$irex_title .=  get_bloginfo('name');
	}
	if ( is_author() ) { 
		global $wp_query;
		$curauth = $wp_query->get_queried_object();	
		$irex_title .= __('Author: ','irex');
		$irex_title .= $curauth->display_name;
		$irex_title .= ' | ';
		$irex_title .= get_bloginfo('name');
	}
	if ( is_single() ) {
		$irex_title .= get_bloginfo('name');
	}
	if ( is_page() && !is_front_page() ) {
		$irex_title .= get_bloginfo('name');
	}
	if ( is_category() ) {
		$irex_title .= get_bloginfo('name');
	}
	if ( is_year() ) { 
		$irex_title .= get_bloginfo('name');
	}
	if ( is_month() ) {
		$irex_title .= get_bloginfo('name');
	}
	if ( is_day() ) {
		$irex_title .= get_bloginfo('name');
	}
	if (function_exists('is_tag')) { 
		if ( is_tag() ) {
			$irex_title .= get_bloginfo('name');
		}				
	}
	if ( is_404() ) {
			$irex_title .= get_bloginfo('name');
		}	
	return $irex_title;
}

/********************************************
 EXCERPT CONTROLL FUNCTION
*********************************************/
function irex_limit_words($string, $word_limit) {
	$words = explode(' ', $string);
	return implode(' ', array_slice($words, 0, $word_limit));
}

/******* theme check fix ***********/
if ( ! isset( $content_width ) ){
    $content_width = 900;
}




