<?php

/********************************************

 Frame Work code starts here

*********************************************/

/********************************************

 Theme Variables

*********************************************/

global $irex_themename;

global $irex_shortname;

require_once('sketch-functions.php'); // pagination, excerpt control etc..

require_once('sketch-theme-support.php'); // Thumbnail supports

require_once('sketch-enqueue.php'); // Enqueue Css Scripts

require_once('sketch-breadcrumb.php'); // custom post types includes

require_once('sketch-custom-navigation.php'); // custom navigation includes

require_once('sketch-background-gallery/skebggallery.php'); // skebggallery includes

require_once('ldc/ldc_main.php'); // like-dislike includes
