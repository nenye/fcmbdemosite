<?php
global $irex_themename;
global $irex_shortname;
/**
 * Filter content with empty post title
 *
 * @since irex
 */
add_filter('the_title', 'irex_untitled');
function irex_untitled($title) {
	if ($title == '') {
		return __('Untitled','irex');
	} else {
		return $title;
	}
}