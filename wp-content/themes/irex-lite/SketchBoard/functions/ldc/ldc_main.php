<?php
define('LDCURL', get_template_directory_uri() . '/SketchBoard/functions/ldc/');


function irex_like_dislike_couter_init_method(){
	if(!is_admin())
    wp_enqueue_script( 'jquery' );
}    
add_action('init', 'irex_like_dislike_couter_init_method');

function like_counter_p($text="Likes: ",$post_id=NULL)
{
	global $post;
	if(empty($post_id)){
		$post_id=$post->ID;
	}
		$ldc_plc1 = get_post_ul_meta($post_id,"ldc_plc");
		if(!isset($ldc_plc1)){ $ldc_plc1 =  0;}
		echo "<span class='ul_cont' onclick=\"alter_ul_post_values(this,'$post_id','ldc_plc')\" >".$text."<div class='like_img'></div>(<span>". $ldc_plc1 ."</span>) <div class='alert-msg'></div></span>";
}

function dislike_counter_p($text="dislikes: ",$post_id=NULL)
{
	global $post;
	if(empty($post_id)){
		$post_id=$post->ID;
	} 
		$ldc_pdc1 = get_post_ul_meta($post_id,"ldc_pdc");
		if(!isset($ldc_pdc1)){ $ldc_pdc1 =  0;}
		echo "<span class='ul_cont' onclick=\"alter_ul_post_values(this,'$post_id','ldc_pdc')\" >".$text."<div class='dislike_img'></div>(<span>". $ldc_pdc1 ."</span>) <div class='alert-msg'></div></span>";
}

function get_post_ul_meta($post_id,$up_type)
{
	$custom = get_post_custom($post_id);
	if(isset($custom[$up_type][0])){ $to_ret = $custom[$up_type][0]; }
	if(empty($to_ret)){
	$to_ret=0;
	}
	return $to_ret;
}

function update_post_ul_meta($post_id,$up_type)
{
	$lnumber=get_post_ul_meta($post_id,$up_type);
	if(!$lnumber)$lnumber=0;

	if(isset($_COOKIE['ul_post_cnt'])){
		$posts=$_COOKIE['ul_post_cnt'];
		array_push($posts,$post_id);
		foreach($posts as $key=>$value){
			setcookie("ul_post_cnt[$key]",$value, time()+1314000);
		}
	}
	else{
		setcookie("ul_post_cnt[0]",$post_id, time()+1314000);
	}
	update_post_meta($post_id,$up_type,$lnumber+1);
}

function irex_wp_dislike_like_footer_script() {
	if(!is_admin())
	{
	?>
	<script type="text/javascript">
	var isProcessing = false; 
	function alter_ul_post_values(obj,post_id,ul_type)
	{
		if (isProcessing)    
		return;           

		isProcessing = true;   
		var count = jQuery(obj).find("span").html().trim();
		
		jQuery(obj).find("span").html("..");
		jQuery.ajax({
		type: "POST",
		url: '<?php echo LDCURL."ajax_counter.php" ?>',
		data: "post_id="+post_id+"&up_type="+ul_type,
		success: function(msg){
			if(count ==  msg.trim()){
				jQuery(obj).find(".alert-msg").empty().html("Already voted!").stop(true,true).fadeIn(400).delay(1000).fadeOut(400);
			}
			else{
				jQuery(obj).find(".alert-msg").empty().html("Thanks for your vote!").stop(true,true).fadeIn(400).delay(1000).fadeOut(400);
			}
			jQuery(obj).find("span").html(msg);
			isProcessing = false;
			}
		});
	}
	</script>
    <?php
    }
}
add_action('wp_footer', 'irex_wp_dislike_like_footer_script');