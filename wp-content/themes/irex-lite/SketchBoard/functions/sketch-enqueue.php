<?php
global $irex_themename;
global $irex_shortname;
/************************************************
*
*  enquque css and javascript
*
************************************************/

//enqueue jquery 
function irex_script_enqueqe() {
	global $irex_shortname;
	if(!is_admin()){	
		wp_enqueue_script('jquery-easing-slider',get_template_directory_uri().'/js/slider/jquery.easing.1.3.js',array('jquery'),'1.2' );
		wp_enqueue_script('jquery-camera-slider',get_template_directory_uri().'/js/slider/camera.min.js',array('jquery'),'1.2' );
		wp_enqueue_script( 'comment-reply' );}		wp_enqueue_script('jquery.slidermobile.js',get_template_directory_uri().'/js/slider/jquery.mobile.customized.min.js',array('jquery'),'1.2' );
}
add_action('wp_enqueue_scripts', 'irex_script_enqueqe');

//enqueue admin css
function irex_theme_stylesheet(){
if ( !is_admin() ) {
		global $wp_version;		$irex_skt_version = NULL;		$theme = wp_get_theme();		$irex_skt_version = $theme['Version'];

	wp_enqueue_script('irex_prettyPhoto_slide',get_template_directory_uri().'/js/jquery.prettyPhoto.js',array('jquery'),true,'1.0' );
	wp_enqueue_script('irex_quicksand_slide',get_template_directory_uri().'/js/jquery.quicksand.js',array('jquery'),true,'1.0' );
	wp_enqueue_script('irex_script_slide',get_template_directory_uri().'/js/component.js',array('jquery'),true,'1.0' );
	wp_enqueue_script('irex_ddsmoothmenusimple_slide',get_template_directory_uri().'/js/ddsmoothmenu.js',array('jquery'),true,'1.0' );
	wp_enqueue_script('irex_tipTip_slide',get_template_directory_uri().'/js/jquery.tipTip.js',array('jquery'),true,'1.0' );	wp_enqueue_style( 'reset-stylesheet', get_template_directory_uri().'/css/reset.css', false, $irex_skt_version );	wp_enqueue_style( 'responsive-stylesheet', get_template_directory_uri().'/css/960_24_col_responsive.css', false, $irex_skt_version );	wp_enqueue_style( 'irex-style', get_stylesheet_uri(), false, $irex_skt_version );
	wp_enqueue_style( 'irex-theme-camera-slider', get_template_directory_uri().'/css/camera.css', false, $irex_skt_version );
	wp_enqueue_style( 'irex-theme-stylesheet', get_template_directory_uri().'/SketchBoard/css/skt-theme-stylesheet.css', false, $irex_skt_version );
	wp_enqueue_style( 'irex-prettyPhoto-stylesheet', get_template_directory_uri().'/css/prettyPhoto.css', false, $irex_skt_version );
	wp_enqueue_style( 'irex-tipTip-stylesheet', get_template_directory_uri().'/css/tipTip.css', false, $irex_skt_version);
	wp_enqueue_style( 'irexddsmoothmenu-theme-stylesheet', get_template_directory_uri().'/css/ddsmoothmenu.css', false, $irex_skt_version );
	wp_enqueue_style( 'irexddsmoothmenu-v-theme-stylesheet', get_template_directory_uri().'/css/ddsmoothmenu-v.css', false, $irex_skt_version );	
	}
}
add_action('wp_enqueue_scripts', 'irex_theme_stylesheet');/* Enqueue New Google Fonts */function irex_load_google_fonts() {				wp_enqueue_style( 'irexfontsjunge','http://fonts.googleapis.com/css?family=Junge' );	wp_enqueue_style( 'irexfontsartifika','http://fonts.googleapis.com/css?family=Artifika' );	wp_enqueue_style( 'irexfontsoxygen','http://fonts.googleapis.com/css?family=Oxygen' );	wp_enqueue_style( 'irexfontsgilda','http://fonts.googleapis.com/css?family=Gilda+Display');}add_action('wp_enqueue_scripts', 'irex_load_google_fonts');
function irex_head(){	global $irex_shortname;	$skt_favicon = "";	$skt_meta = '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />'."\n";		if(sketch_get_option($irex_shortname.'_favicon')){		$skt_favicon = sketch_get_option($irex_shortname.'_favicon','biznez');		$skt_meta .= "<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"$skt_favicon\"/>\n";	}	echo $skt_meta;		if ( is_front_page() && 'page' == get_option( 'show_on_front' ) ) {	?>		<style type="text/css">			#header{ padding: 0 0 40px;} 		</style>	<?php 	}}add_action('wp_head', 'irex_head');
//enqueue footer script 
function irex_footer_script() {
	global $irex_shortname;
	if((!is_admin() && is_front_page()) && 'page' == get_option( 'show_on_front' ) )
	{
		require_once(get_template_directory().'/js/camera-slider-config.php');
	}    	 if (is_page_template('template-fullwidthgallery.php') ) { ?>		<script type="text/javascript">			jQuery(document).ready(function(){				show_skebg_thumbs();			});		</script>	<?php } ?>		<!-- Color Skings CSS -->	<?php $color_scheme=(sketch_get_option($irex_shortname."_color_scheme"))? sketch_get_option($irex_shortname."_color_scheme") :'green' ;?>	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/skins/<?php echo $color_scheme;?>/<?php echo $color_scheme;?>.css" type="text/css" media="screen" /><!-- /Color Skings CSS -->
<?php }
add_action('wp_footer', 'irex_footer_script');