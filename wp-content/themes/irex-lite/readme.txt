== Copyright and License ==
Irex Lite WordPress Theme, Copyright (C) 2013 SketchThemes
Irex Lite WordPress theme is licensed under the GPL.

==Credits ===
Theme Irex lite uses:

* The script jquery.easing.1.3.js v1.3.0 is licensed under the BSD License.
* The script jquery.quicksand.js v1.2.2 is licensed under the MIT and GPL version 2 licenses.(http://github.com/razorjack/quicksand
)
* The script jquery.prettyPhoto.js v3.1.4 is licensed under the "GPLv2" or "Creative Commons 2.5 license".
* The script jcamera.min.js v1.3.3 is Licensed under the MIT license.
* The script jquery.tipTip.js v1.3 is dual licensed under the MIT and GPL licenses.(code.drewwilson.com/entry/tiptip-jquery-plugin)
* Google Webfonts: 
	'Junge' is licensed under SIL Open Font License,1.1.(http://www.google.com/fonts/specimen/Junge)
	'Artifika' is licensed under SIL Open Font License,1.1.(http://www.google.com/fonts/specimen/Artifika)
	'Oxygen' is licensed under SIL Open Font License,1.1.(http://www.google.com/fonts/specimen/Oxygen)
	'Gilda Display' is licensed under SIL Open Font License,1.1.(http://www.google.com/fonts/specimen/Gilda+Display)
	
* The images is a free public picture from pixabay and distributed under the terms of the Creative Commons CC0 License.	

Image link : 
			http://pixabay.com/en/architecture-big-ben-british-bus-2656/
			http://pixabay.com/en/krak%C3%B3w-street-tram-light-114256/
			http://pixabay.com/en/new-york-times-square-night-view-115627/
			http://pixabay.com/en/singapore-night-evening-skyscrapers-86694/
			http://pixabay.com/en/city-guwahati-assam-india-night-169926/

* Timeliner_Modeling_Demo.png
	http://pixabay.com/en/girl-woman-exotic-beauty-model-97433/
	http://pixabay.com/en/girl-model-pretty-portrait-lady-97088/

* irex-mac-420px.png
	http://pixabay.com/en/couple-bride-love-wedding-bench-260899/

* fullscreen-mac-420px.png
	http://pixabay.com/en/girl-woman-sexy-lingerie-posing-254708/

* bizstudio-default-demo.png
	http://pixabay.com/en/beach-beautiful-blue-female-girl-15689/
	http://pixabay.com/en/sea-blue-sky-sand-beach-holiday-142459/

* biznez-career-counseling-demo.png
	http://unsplash.s3.amazonaws.com/batch%203/doctype-hi-res.jpg

* Advertica_screen_420px.png
	http://666a658c624a3c03a6b2-25cda059d975d2f318c03e90bcf17c40.r92.cf1.rackcdn.com/unsplash_522b53fb137bb_1.jpg

* sketchmini-mac-420px.png
	http://pixabay.com/en/blue-summer-woman-mom-people-joy-69762/
	http://pixabay.com/en/couple-people-girlfriend-boyfriend-17102/
	http://pixabay.com/en/red-people-sunlight-promenade-211709/

* Invert-mac-300px.png
	The Slider Image was made in Photoshop from scratch. Hence No Attribution is required.

* Analytical_Interior_Demo.png
	http://publicdomainarchive.com/girl-nature-outdoors-autumn-lens-flare-sun-rays/


* Foodeez-Theme.png
	http://pixabay.com/en/restaurant-dinner-tables-yowani-237060/

all Image link is a free public picture from pixabay and distributed under the terms of the Creative Commons CC0 Universal Public Domain Dedication (http://creativecommons.org/publicdomain/zero/1.0/deed.en).
All other images were created by sketchthemes as per requirement.


== Changelog ==

= 1.0.13 =
* WP 3.9.1 Compatible.

= 1.0.12 =
* WP 3.8.2 Compatible.

= 1.0.11 =
* Fixed Full width background Gallery issue.

= 1.0.10 =
* Change Theme Tags.
* Fixed Full width background Gallery issue.

= 1.0.9 =
* WP 3.8.1 Compatible.
* Update screenshot.png file.
* update few backend styling. 

= 1.0.8 =
* WP 3.8 Compatible.
* update backend style. 

= 1.0.7 =
* Removed redirection to theme options page on activating theme.

= 1.0.6 =
* Fixed Front page content issue.
* fixed logo width.
* WP 3.7.1 Compatible.
* Remove Blog title from Single post page

= 1.0.5 =
* Change Theme Description.

= 1.0.4 =
* Fixed default "Logo ALT Text" value.

= 1.0.3 =
* Fixed theme defaults and registers the various WordPress function into one setup function hooked into after_setup_theme action.
* WP 3.6.1 Compatible.

= 1.0.2 =
* Change prefix of all custom functions and global variables.
* google fonts enqueued with wp_enqueue_scripts.

= 1.0.1 =
* Fixed site title and description instead of website logo issue.
* Fixed ordered and unordered lists styling.
* Fixed social icons displayed issue even if social media url is empty.
* Fixed long post title issue.
* Change prefix of all custom functions with unique prefixes.


= 1.0.0 =
* Initial release
 
