<?php

// Do not delete these lines

	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))

		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>

		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.','irex'); ?></p>

	<?php

		return;

	}

?>

<!-- You can start editing here. -->

<?php if ( have_comments() ) : ?>

	<h2 id="comments"><?php _e('Comments','irex'); ?></h2>

	<ul class="commentlist">

  	<?php wp_list_comments(array( 'callback' => 'irex_comment' )); ?>

  	</ul>

	<div class="navigation">

		<div class="alignleft"><?php previous_comments_link() ?></div>

		<div class="alignright"><?php next_comments_link() ?></div>

		<div class="fix"></div>

	</div>

<?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>

		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>

<div id="comments_wrap">

		<!-- If comments are closed. -->

		<p class="nocomments"><?php _e('Comments are closed.','irex'); ?></p>

</div> <!-- end #comments_wrap -->

	<?php endif; ?>

<?php endif; ?>

<div id="responds">

<?php if ('open' == $post->comment_status) : ?>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>

<p><?php _e('You must be','irex'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in','irex'); ?></a> <?php _e('to post a comment.','irex'); ?></p>

<?php else : ?>

<?php

$aria_req ='';

$message ='<div class="field clearfix">

	<label for="comment">'. __('Message','Seleicon') .'</label>

	<div id="input"><textarea class="textarea" name="comment" id="comment" tabindex="4"></textarea></div>

</div>';

$comment_args = array( 'fields' => apply_filters( 'comment_form_default_fields', array(

           'author' => '' .

                       '<div class="field clearfix"><label for="author">' . __('Name','Seleicon') . '</label> ' .

                       ( $req ? '<span class="required">*</span>' : '' ) .

                       '<div id="input"><input id="author" name="author" class="text" type="text"  value="" tabindex="1" size="22"' . $aria_req . ' /></div>' .

                       '</div><!-- #form-section-author .form-section -->',

           'email'  => '' .

                       '<div class="field clearfix"><label for="email">' . __( 'Email','Seleicon') . '</label> ' .

                       ( $req ? '<span class="required">*</span>' : '' ) .

                       '<div id="input"><input id="email" name="email" class="text" type="text"  value="" size="22"  tabindex="2"' . $aria_req . ' /></div>' .

               '</div><!-- #form-section-email .form-section -->',

		  'url' => '' .

                       '<div class="field clearfix"><label for="url">' . __( 'Website','Seleicon' ) . '</label> ' .

                       ( $req ? '<span class="required">*</span>' : '' ) .

                       '<div id="input"><input class="text" type="text" name="url" id="url" value="" size="22" tabindex="3"' . $aria_req . ' /></div>' .

                       '</div><!-- #form-section-author .form-section -->',

            ) ),

           'comment_field' => '' .

                       $message .

                       '<!-- #form-section-comment .form-section -->',

           'comment_notes_after' => '',

       );

       comment_form($comment_args);

?>

<?php endif; // If registration required and not logged in ?>

<?php endif; // if you delete this the sky will fall on your head ?>

<?php //comment_form(); ?>

</div> <!-- end #respond -->