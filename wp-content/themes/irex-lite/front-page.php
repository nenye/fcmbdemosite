<?php
if ( 'page' == get_option( 'show_on_front' ) ) {
	get_header();
	global $irex_shortname; 
?>
<!-- #Container Area -->

<div id="container" class="clearfix">
  <div class="wrapper container_24 clearfix">
    <?php if(is_front_page()){ ?>
    <!-- #Slider -->
    <div id="skt_slider" class="container_24" style="overflow:hidden;">
      <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
	  
       <?php if(sketch_get_option($irex_shortname.'_slider_img1')) { ?> <div data-thumb="<?php if(sketch_get_option($irex_shortname.'_slider_img1')) { echo sketch_get_option($irex_shortname.'_slider_img1'); } ?>" data-src="<?php if(sketch_get_option($irex_shortname.'_slider_img1')) { echo sketch_get_option($irex_shortname.'_slider_img1'); } ?>"> 
          <?php if(sketch_get_option($irex_shortname.'_content_slider1')) { ?><div class="camera_caption fadeFromBottom">
            <?php if(sketch_get_option($irex_shortname.'_content_slider1')) { echo sketch_get_option($irex_shortname.'_content_slider1'); } else { echo 'The text of your caption'; } ?>
          </div> <?php } ?>
        </div><?php } ?>
        <?php if(sketch_get_option($irex_shortname.'_slider_img2')) { ?><div data-thumb="<?php if(sketch_get_option($irex_shortname.'_slider_img2')) { echo sketch_get_option($irex_shortname.'_slider_img2'); } ?>" data-src="<?php if(sketch_get_option($irex_shortname.'_slider_img2')) { echo sketch_get_option($irex_shortname.'_slider_img2'); } ?>">
          <?php if(sketch_get_option($irex_shortname.'_content_slider2')) { ?><div class="camera_caption fadeFromBottom">
            <?php if(sketch_get_option($irex_shortname.'_content_slider2')) { echo sketch_get_option($irex_shortname.'_content_slider2'); } else { echo 'The text of your caption'; } ?>
          </div><?php } ?>
        </div><?php } ?>
        <?php if(sketch_get_option($irex_shortname.'_slider_img3')) { ?><div data-thumb="<?php if(sketch_get_option($irex_shortname.'_slider_img3')) { echo sketch_get_option($irex_shortname.'_slider_img3'); } ?>" data-src="<?php if(sketch_get_option($irex_shortname.'_slider_img3')) { echo sketch_get_option($irex_shortname.'_slider_img3'); } ?>">
          <?php if(sketch_get_option($irex_shortname.'_content_slider3')) { ?><div class="camera_caption fadeFromBottom">
            <?php if(sketch_get_option($irex_shortname.'_content_slider3')) { echo sketch_get_option($irex_shortname.'_content_slider3'); } else { echo 'The text of your caption'; } ?>
          </div><?php } ?>
        </div><?php } ?>
		
      </div>
    </div>
    
    <!-- #Slider -->
    <?php } ?>
    
    <!-- Wecome Heading -->
    <div class="welcome_head clearfix">
      <?php if(sketch_get_option($irex_shortname."_mid_text")){ ?>
      <span><?php echo sketch_get_option($irex_shortname."_mid_text"); ?></span>
      <?php } ?>
    </div>
    
    <!-- Wecome Heading --> 
    
    <!------ Content ------>
    
    <div id="front_page_content">
      <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
      <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
          <?php //the_content(); ?>
        </div>
      </div>
      <?php endwhile; ?>
      <?php else : ?>
      <div class="post">
        <h2>
          <?php _e('Not Found','irex'); ?>
        </h2>
      </div>
      <?php endif; ?>
      <!-- Latest Featured Wrap -->
      <div class="latest_project container_24">
        <div class="project_head">
          <?php if(sketch_get_option($irex_shortname.'_feature_head_area')) { echo sketch_get_option($irex_shortname.'_feature_head_area'); } ?>
        </div>
        <ul class="preview">
          <!-- Featured Box 1 -->
          <li class="latest_project_box grid_6 alpha omega">
            <div class="feature_image" style="position: relative;">
              <?php ?>
              <a href="<?php if(sketch_get_option($irex_shortname.'_feature_img1')) { echo sketch_get_option($irex_shortname.'_feature_img1'); } ?>" rel="prettyPhoto[preview]" title=""> <img src="<?php if(sketch_get_option($irex_shortname.'_feature_img1')) { echo sketch_get_option($irex_shortname.'_feature_img1'); } ?>" alt=""/> <span></span> </a> <a href="<?php if(sketch_get_option($irex_shortname.'_feature_link1')) { echo sketch_get_option($irex_shortname.'_feature_link1'); } ?>" class="tooltip fullpostlink" title="Full Post"></a> </div>
            <div class="title"><a href="<?php if(sketch_get_option($irex_shortname.'_feature_link1')) { echo sketch_get_option($irex_shortname.'_feature_link1'); } ?>" title="">
              <?php if(sketch_get_option($irex_shortname.'_feature_text1')) { echo sketch_get_option($irex_shortname.'_feature_text1'); } ?>
              </a></div>
          </li>
          <!-- Featured Box 1 --> 
          <!-- Featured Box 2 -->
          <li class="latest_project_box grid_6 alpha omega">
            <div class="feature_image" style="position: relative;">
              <?php ?>
              <a href="<?php if(sketch_get_option($irex_shortname.'_feature_img2')) { echo sketch_get_option($irex_shortname.'_feature_img2'); } ?>" rel="prettyPhoto[preview]" title=""> <img src="<?php if(sketch_get_option($irex_shortname.'_feature_img2')) { echo sketch_get_option($irex_shortname.'_feature_img2'); } ?>" alt=""/> <span></span> </a> <a href="<?php if(sketch_get_option($irex_shortname.'_feature_link2')) { echo sketch_get_option($irex_shortname.'_feature_link2'); } ?>" class="tooltip fullpostlink" title="Full Post"></a> </div>
            <div class="title"><a href="<?php if(sketch_get_option($irex_shortname.'_feature_link2')) { echo sketch_get_option($irex_shortname.'_feature_link2'); } ?>" title="">
              <?php if(sketch_get_option($irex_shortname.'_feature_text2')) { echo sketch_get_option($irex_shortname.'_feature_text2'); } ?>
              </a></div>
          </li>
          <!-- Featured Box 2 --> 
          <!-- Featured Box 3 -->
          <li class="latest_project_box grid_6 alpha omega">
            <div class="feature_image" style="position: relative;">
              <?php ?>
              <a href="<?php if(sketch_get_option($irex_shortname.'_feature_img3')) { echo sketch_get_option($irex_shortname.'_feature_img3'); } ?>" rel="prettyPhoto[preview]" title=""> <img src="<?php if(sketch_get_option($irex_shortname.'_feature_img3')) { echo sketch_get_option($irex_shortname.'_feature_img3'); } ?>" alt=""/> <span></span> </a> <a href="<?php if(sketch_get_option($irex_shortname.'_feature_link3')) { echo sketch_get_option($irex_shortname.'_feature_link3'); } ?>" class="tooltip fullpostlink" title="Full Post"></a> </div>
            <div class="title"><a href="<?php if(sketch_get_option($irex_shortname.'_feature_link3')) { echo sketch_get_option($irex_shortname.'_feature_link3'); } ?>" title="">
              <?php if(sketch_get_option($irex_shortname.'_feature_text3')) { echo sketch_get_option($irex_shortname.'_feature_text3'); } ?>
              </a></div>
          </li>
          <!-- Featured Box 3 --> 
          <!-- Featured Box 4 -->
          <li class="latest_project_box grid_6 alpha omega">
            <div class="feature_image" style="position: relative;">
              <?php ?>
              <a href="<?php if(sketch_get_option($irex_shortname.'_feature_img4')) { echo sketch_get_option($irex_shortname.'_feature_img4'); } ?>" rel="prettyPhoto[preview]" title=""> <img src="<?php if(sketch_get_option($irex_shortname.'_feature_img4')) { echo sketch_get_option($irex_shortname.'_feature_img4'); } ?>" alt=""/> <span></span> </a> <a href="<?php if(sketch_get_option($irex_shortname.'_feature_link4')) { echo sketch_get_option($irex_shortname.'_feature_link4'); } ?>" class="tooltip fullpostlink" title="Full Post"></a> </div>
            <div class="title"><a href="<?php if(sketch_get_option($irex_shortname.'_feature_link4')) { echo sketch_get_option($irex_shortname.'_feature_link4'); } ?>" title="">
              <?php if(sketch_get_option($irex_shortname.'_feature_text4')) { echo sketch_get_option($irex_shortname.'_feature_text4'); } ?>
              </a></div>
          </li>
          <!-- Featured Box 4 -->
          
        </ul>
      </div>
      <!-- Latest Featured Wrap --> 
    </div>
    <!------ Content ------> 
	<div class="frontcont entry">
        <?php the_content(); ?>
	</div>
  </div>
</div>
<!-- #Container Area -->
<?php get_footer(); ?>
<?php 
} else {
	include( get_home_template() );
}
 ?>