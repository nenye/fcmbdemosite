<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fcmbdemo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~Gx#3Nb$:/t]`Nq:DeiJ%u9Ei<7?~~D`Lz!l0(h|TDpSGRiv4_t<3!8#]3MJ(<G<');
define('SECURE_AUTH_KEY',  '5C6^-_iZm6v-Y6C}ychg-*t?(P]pjdjk8?BEgqZ)P)q_X(B4fV[b8;[nMl3fT/_5');
define('LOGGED_IN_KEY',    'L)hAqCN[G];&1~(U@8x~viS43)q`g5YhJ#tNjz=Uzxhs)!9Ym^x|0whV)P6-yK;P');
define('NONCE_KEY',        'U.m2(l#K k p0qIv(4.ozmK6S/w_1Mu+l^rGgiG&{k7Bs7hPbQ1p@U.)}c<e]E*~');
define('AUTH_SALT',        '.i64r[Yy`%e(X8b?)o*GMe&$w:~;#a^==3-QcoYo|S/{fQ5CQ:+LQLZxX73bHT9X');
define('SECURE_AUTH_SALT', '2Db{AIq~4E%+JA{54r)TO~V8p;7.Unq`CT2+Ff~u-,=j-.[eFWTA7yGYfttvqxA1');
define('LOGGED_IN_SALT',   'hd[zn.5}Oxt`R},yjJBO}h# T~TkiL_~nk7B^2U9w:~^2b^ONCwSv16s^:@O/rq2');
define('NONCE_SALT',       'GzF+Y<FIm1R[V[?fsQIX%hFDk?3X )[`l?hIO%,yUV}#gVZh~>6 Kil[h(4>ND)d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fcmb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
